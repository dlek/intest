# intest - framework for inspection testing

`intest` is a framework for running inspection tests such as linters and
return or display results in a simple and straightforward manner.  Tests are
implemented as simple modules which implement predictable functions which are
called directly.  All tests are implemented as Bash scripts.

By default, testing candidates are discovered using `git ls-files` if the
`git` program is available; otherwise `find` is used.

## Running intest

Usage as reported by the script itself:

```
Usage: intest -h|--help
       intest [-D|--testdir dir] test ...

Executes tests defined in test script definitions contained in "test"
arguments.  Intended for inspection-based testing such as linters.

By default, the directory where intest resides is searched for a given test.
Additional directories may be provided with one or more uses of the "-D"
argument.  These will be searched for the matching script before the default
directory.
```

## Supported modules

Currently supported:

* [shellcheck](https://www.shellcheck.net/) checks shell scripts for bugs or
  potential pitfalls.
* [pylint](https://www.pylint.org/) checks Python scripts for bugs and overall
  quality aspects such as readability and style.
* [yamllint](https://github.com/adrienverge/yamllint) checks YAML files.
* unixmode checks that text files end in \n, not \r\n.

Creating new modules should be pretty simple for somebody familiar with Bash
or similar POSIX shell scripting.

## Writing modules

Modules implement specific functions to find files of a specific type,
determine whether those files are eligible for testing, and to run the test
against each file.  These functions are named according to the test.  The
required functions are:

* `${test}_supported()` returns whether the command supporting the test is
  available.
* `${test}_eligible_files()` finds candidate files.  Modules should use the
  provided `ls_files()` function to provide the initial list which can then be
  filtered as necessary (see later in this section for details).
* `${test}_enabled_for()` takes a file argument and returns whether the file
  is eligible for testing.  This will typically mean it contains a header
  containing a directive for the linter.  The function may return (via stdout)
  options for testing.
* `${test}_file()` takes a file argument and possibly options and runs the
  linter or other command against it.

A skeleton is available in `extras/module.skel` which provides a basic
outline.

Modules can be placed in the intest directory, or in another directory if
specified with the `-D` option.  Contributions are welcome.

The `ls_files()` function optionally accepts file blobs as arguments, in the
same manner as `git ls-files`, which is its default implementation if the
`git` command is present in the environment; otherwise, a basic `find` is
used.  On invocation of `intest` an arbitary function can be specified by
assigning its name to the environment variable `$INTEST_LS_FILES`.

## Caching

To avoid repeated testing of files which are known to pass, a simple caching
mechanism is used.  Once a file passes, an empty file is created for it in
this cache.  This file is then skipped on subsequent runs, until it is updated
or the cache is cleared.

The cache by default resides in the current directory as `.lintcache`.  To
clear the cache, simply issue `rm .lintcache/*`.

## Output

Here is an example running on this repository.

```
$ ./intest shellcheck pylint yamllint
>>>>  shellcheck beginning
>>>>    Loading ./shellcheck...
>>>>    Checking shellcheck is supported: Yes
>>>>    Determining eligible files: 4
>>>>    Running for shellcheck-enabled files
 cached intest
 passed pylint
 cached shellcheck
skipped yamllint
>>>>  shellcheck finished: 1 passed, 3 skipped, and 0 failed
>>>>  pylint beginning
>>>>    Loading ./pylint...
>>>>    Checking pylint is supported: Yes
>>>>    Determining eligible files: 0
>>>>  yamllint beginning
>>>>    Loading ./yamllint...
>>>>    Checking yamllint is supported: Yes
>>>>    Determining eligible files: 1
>>>>    Running for yamllint-enabled files
 cached .gitlab-ci.yml
>>>>  yamllint finished: 0 passed, 1 skipped, and 0 failed
```

One of these tests was actually subverted: the `shellcheck` directive in the
`yamllint` module was temporarily removed in order to demonstrate the results
for a file which hasn't been enabled for testing.

There are three tests specified as arguments to the framework.  There are no
python scripts available for testing as this framework is all shell scripts,
so the count of eligible files for this test is 0.

There is one candidate for `yamllint`, but it passed previous testing and is
skipped with the status `cached`.

Under the `shellcheck` test, two files were previously passed and are skipped;
`pylint` is checked, and `yamllint` is skipped as it did not contain a
directive in its header (in fact it was temporarily removed to show this
output).

Each test contains a summary line indicating result counts.

The output of the testing command is not filtered, but should only be present
in case of warnings and errors, and appropriate options should be passed to
the testing command to ensure this is the case.

## Eligible but untestable files

If a test is invoked, but not supported (i.e. `${test}_supported()` returns
false) the search for eligible files is still performed and if candidates
exist a warning is issued.
