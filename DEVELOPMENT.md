# Development notes

## Docker image

The Docker image in `extra/` can be built using the Makefile there:

```shell
$ cd extra
$ make
```

It's pretty basic.

From time to time one of the utilities will need to be updated.  When this is
done it is a good idea to test the intest code against itself.  To do this and
test the Docker image at the same time, one can do this from the repository
root:

```shell
$ docker run -v $(pwd):/workdir -w /workdir dlek/tester ./intest shellcheck
```

This assumes you have the latest image locally, which you will if you have
just built it (otherwise use `docker pull dlek/tester`).
